$CB = Get-WmiObject -Class Win32_Product | Where Name -Like "*Carbon*" | Select-Object -Property Name,Version
$CBCount = $CB | Measure-Object
$CB2 = Get-ItemProperty HKLM:\Software\Wow6432Node\Microsoft\Windows\CurrentVersion\Uninstall\* | Where DisplayName -Like "*Carbon*" | Select-Object DisplayName, DisplayVersion
$CB2Count = $CB2 | Measure-Object

$CBTotal = $($CBCount.Count) + $($CB2Count.Count)

if ( $($CBTotal) -lt 2) { 
    Write-Host "MISSING CARBON BLACK"
    $CB
    $CB2
} else {
    Write-Host "OK"
}
