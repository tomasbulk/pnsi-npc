#------
# Verify the running powershell instance has admin rights
function Verify-AdministratorPrivilages {
    $IsAdmin=[Security.Principal.WindowsIdentity]::GetCurrent()
    
    If ((New-Object Security.Principal.WindowsPrincipal $IsAdmin).IsInRole([Security.Principal.WindowsBuiltinRole]::Administrator) -eq $FALSE)
    {
        "`nERROR: Run this script after logging on as an admin."

        Read-Host -Prompt "Press any key to exit"
        exit
    }
}

#------
# Set the timezone to Central Standar
function Set-TimezoneFunc {
    Set-Timezone -Id "Central Standard Time" -PassThru
    Write-Host "Timezone set to Central Standard Time"
}

#------
# Change the sleep settings to never go to sleep while plugged in
function Change-SleepSettings {

    powercfg.exe /change disk-timeout-ac 0
    Write-Host "Disk Timeout set to 0"
    
    powercfg.exe /change standby-timeout-ac 0
    Write-Host "Standby Timeout set to 0"

    powercfg.exe /change hibernate-timeout-ac 0
    Write-Host "Hibernate Timeout set to 0"

}

#------
# Install all the apps in .\packagelist.json via winget
function App-Install {
    winget import --import-file packagelist.json
}

#------
# Change the current users password
function Change-Password {
    $Password = Read-Host -Prompt "Enter the wanted password for the current user " -AsSecureString
    $UserAccount = Get-LocalUser -Name $env:UserName
    $UserAccount | Set-LocalUser -Password $Password 
}

#------
# Change the domain and system name (or just one)
function Set-DomainSystemName {
    Write-Host "Select an option: "
    Write-Host ""
    Write-Host "(1) Add computer to a domain"
    Write-Host "(2) Change the computer name"
    Write-Host "(3) Do both"
    Write-Host "(4) Do nothing"
    $Choice = Read-Host -Prompt "(1-4) "

    if ( $choice -eq 1 )
    {
        $DomainName = Read-Host -Prompt "Which domain are you joining this to? "
        Add-Computer -DomainName $DomainName -Credential $DomainName\administrator
    }

    if ( $choice -eq 2 )
    {
        $NewName = Read-Host -Prompt "What would you like to name this PC? "
        Rename-Computer -NewName $NewName
    }

    if ( $choice -eq 3 )
    {
        $NewName = Read-Host -Prompt "What would you like to name this PC? "
        $DomainName = Read-Host -Prompt "Which domain are you joining this to? "
        Add-Computer -NewName $NewName -DomainName $DomainName -Credential $DomainName\administrator
    }
}

#------
# Get the current installed version of Dell Command Updates
function Get-DCUVersion {
    Write-Host "Querying for Dell Command Update..."
    $DCUVersion = Get-WmiObject -Class Win32_Product | where name -Like "Dell Command*Update*"
    if ( $DCUVersion -ne $null )
    {
        Write-Host "Found DCU Version $($DCUVersion.Version)"
    }
    return $DCUVersion
}

#------
# Return the system architecture
function Get-Architecture {
	
	[CmdletBinding()]
	param ()
	
	$Architecture = Get-WmiObject -Class Win32_OperatingSystem | Select-Object OSArchitecture
	$Architecture = $Architecture.OSArchitecture
	#Returns 32-bit or 64-bit
	Return $Architecture
}

#------
# Get the location of the Dell Command Update CLI application
function Get-DellCommandUpdateLocation {
	
	$Architecture = Get-Architecture
	If ($Architecture -eq "64-bit") {
        $File = Get-ChildItem -Path ${env:ProgramFiles(x86)} -Filter "dcu-cli.exe" -ErrorAction SilentlyContinue -Recurse
	} else {
		$File = Get-ChildItem -Path $env:ProgramFiles -Filter "dcu-cli.exe" -ErrorAction SilentlyContinue -Recurse
	}
    If ( $File -eq $null ) {
        $File = Get-ChildItem -Path $env:ProgramFiles -Filter "dcu-cli.exe" -ErrorAction SilentlyContinue -Recurse
    }
	Return $File.FullName
}

#------
# Add, remove, and get the state of the Microsoft App Store

function Remove-MicrosoftStore {
    Get-AppxPackage *windowsstore* | remove-appxPackage
}

function Add-MicrosoftStore {
    Get-AppxPackage -allusers *WindowsStore* | Foreach {Add-AppxPackage -DisableDevelopmentMode -Register “$($_.InstallLocation)\AppXManifest.xml”}
}

function Get-MicrosoftStore {
    $state = Get-AppxPackage *windowsstore*
    Return $state
}

#------
# Add, remove, and get the state of Quick Assist

function Remove-QuickAssist {
    Remove-WindowsCapability -online -name App.Support.QuickAssist~~~~0.0.1.0
}

function Add-QuickAssist {
    Add-WindowsCapability -online -name App.Support.QuickAssist~~~~0.0.1.0
}

function Get-QuickAssist {
    $state = Get-WindowsCapability -online -name App.Support.QuickAssist~~~~0.0.1.0
    Return $state.State 
}

#------
# Manage the Microsoft Store

function Microsoft-Store {
    $MicrosoftStore = $(Get-MicrosoftStore)

    if ($MicrosoftStore -eq $null ) {
        Write-Host "Windows Store is NOT installed."
        $confirmation = Read-Host "Do you want to Install Microsoft Store? (y/n) "
        if ($confirmation -eq 'y') { Add-MicrosoftStore }
    } else {
        Write-Host "Windows Store is installed."
        $confirmation = Read-Host "Do you want to remove Microsoft Store? (y/n)"
        if ($confirmation -eq 'y') { Remove-MicrosoftStore }
    }
}

#------
# Manage Quick Assist

function Quick-Assist {
    $QuickAssist = $(Get-QuickAssist)

    if ( $QuickAssist -eq "Installed" ) { 
        Write-Host "Quick Assist is installed."
        $confirmation = Read-Host "Do you want to remove Quick Assist? (y/n) "
        if ($confirmation -eq 'y') { Remove-QuickAssist }
    }
    else { 
        Write-Host "Quick Assist is NOT installed."
        $confirmation = Read-Host "Do you want to install Quick Assist? (y/n) "
        if ($confirmation -eq 'y') { Add-QuickAssist }
    }
}
