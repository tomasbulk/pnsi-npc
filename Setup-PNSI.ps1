Import-Module .\PNSI-Module.psm1 -DisableNameChecking

# Verify that user running script is an administrator
Verify-AdministratorPrivilages

## Set Timezone
Set-TimezoneFunc

## Sleep Settings
Change-SleepSettings

## WINGET
$confirmation = Read-Host "Install all programs? (NOTE: WinGet must be installed.) (y/n) "
if ($confirmation -eq 'y') { App-Install }

## PNSI-ADMIN Password
$confirmation = Read-Host "Would you want change the current user's password? (y/n) "
if ($confirmation -eq 'y') { Change-Password }

## Add to Domain and/or change computer name
Set-DomainSystemName

# Security_Hardening
Microsoft-Store
Quick-Assist

## Dell Command Update Updates
$DCU_Version = Get-DCUVersion
$DCU_EXE=Get-DellCommandUpdateLocation
$confirmation = Read-Host "Do you want to run Dell Command Updates? (y/n) "
if ( $confirmation -eq 'y' )
{
    if ( $($DCU_Version.Version) -ge 3 ){
        & $DCU_EXE /applyUpdates
    }
    else {
        Write-Host "This version of DCU is unsupported. Please install the latest version."
    }
    Write-Host " "
}

# Restrict ExecutionPolicy
Write-Host "Setting ExecutionPolicy back to Restricted..."
Set-ExecutionPolicy Restricted

## RESTART
$confirmation = Read-Host "Do you want to reboot? (y/n) "
if ($confirmation -eq 'y')
{
    Write-Host "Restarting Computer..."
    Restart-Computer
}
