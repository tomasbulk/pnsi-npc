#---
# Require ActiveDirectory Module
$state = Get-WindowsCapability -Online -Name 'Rsat.ActiveDirectory.DS-LDS.Tools~~~~0.0.1.0'
if ($state.State -ne 'Installed') {
    Add-WindowsCapability -Online -Name 'Rsat.ActiveDirectory.DS-LDS.Tools~~~~0.0.1.0'
} 
Import-Module ActiveDirectory

#---
$computer = $env:COMPUTERNAME
$group = 'EnableBluetoothAccess'
$BluetoothDrivers = GWMI Win32_PNPEntity -Filter "caption like '%bluetooth%'"

$members = Get-ADGroupMember -Identity $group -Recursive | Select -ExpandProperty Name

If ($members -contains $computer) {
    Write-Host "$computer is a member of $group"
    Write-Host "Making sure $computer can use bluetooth..."

    # enable bluetooth devices
    foreach ( $ID in $($BluetoothDrivers.PNPDeviceID) ) {
        Enable-PnpDevice -InstanceId $ID -confirm:$false
    }

    # sleep for 4 seconds
    Start-Sleep 4

    # run enable once more - enable main bluetooth driver makes other drivers visible, need to enable these
    $BluetoothDrivers = GWMI Win32_PNPEntity -Filter "caption like '%bluetooth%'"
    foreach ( $ID in $($BluetoothDrivers.PNPDeviceID) ) {
        Enable-PnpDevice -InstanceId $ID -confirm:$false
    }

    Write-Host "Bluetooth enabled"
} Else {
    Write-Host "$computer is not a member of $group"
    Write-Host "Making sure $computer cannot use bluetooth..."

    #disable bluetooth devices
    foreach ( $ID in $($BluetoothDrivers.PNPDeviceID) ) {
        Disable-PnpDevice -InstanceId $ID -confirm:$false
    }

    Write-Host "Bluetooth disabled"
}
