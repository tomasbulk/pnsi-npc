Import-Module .\PNSI-Module.psm1 -DisableNameChecking

Write-Host "Script will only work with Dell Command Update 3.0+"
Write-Host "Please ensure you have that installed..."

## Dell Command Update Updates
$DCU_EXE=Get-DellCommandUpdateLocation
& $DCU_EXE /applyUpdates
Write-Host " "


Write-Host "Setting ExecutionPolicy back to Restricted..."
Set-ExecutionPolicy Restricted
