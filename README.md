# PNSI New PC Script ![PNSI Logo](image.png)

## Simple Powershell script to install basic programs for customers.
Multiple scripts are included in this repository, and one large script to bring it all together.

### Setup-PNSI.ps1 does the following:
- With [winget](https://devblogs.microsoft.com/commandline/windows-package-manager-1-0/):
    - Install Adobe Reader
    - Installs Chrome
    - Installs Firefox
    - Installs 7-Zip
    - Installs VLC
    - Installs Java 8
    - Install Dell Command Update
- Set the Timezone to Central Standard
- Set the Sleep Settings to:
    - Disk Timeout 0
    - Standby Timeout 0
    - Hibernate Timeout 0
- Changes the current user password
- Change system name
- Add system to domain
- Harden System Security
    - Removes or reinstall Microsoft Store
    - Removes or reinstall Quick Assist
- Run Dell Command Update

### Update-DellFirmware.ps1 does the following:
- Run Dell Command Updates.

### Harden-Security.ps1 does the following:
- Removes or reinstall Microsoft Store
- Removes or reinstall Quick Assist

### Install Guide
- Download the zip file of the git repo.
- Extract.

### Prerequisites:
#### [Winget](https://devblogs.microsoft.com/commandline/windows-package-manager-1-0/)
Press ctrl+r and type in: 
```
ms-appinstaller:?source=https://aka.ms/getwinget
```
This will install winget, an necessary tool.

### Running
- Open a powershell prompt *as an administrator* and *NOT in (x86) mode* (Script may fail otherwise).
- Run the following commmand:
```powershell
Set-ExecutionPolicy Unrestricted
```
- Then run a script of your choice:
ex:
```powershell
.\Setup-PNSI.ps1
```
